"""
Programming for linguists

Implementation of the data structure "Stack"
"""

from typing import Iterable


class Stack:
    """
    Stack Data Structure
    """

    def __init__(self, data: Iterable = None):
        if data:
            self.data = list(data)
        else:
            self.data = []


    def push(self, element):
        self.data.append(element)
        """
        Add the element ‘element’ at the top of stack
        :param element: element to add to stack
        """

    def pop(self):
        if self.empty() == True:
            raise ValueError
        else:
            self.data.pop(-1)
        """
        Delete the element on the top of stack
        """

    def top(self):
        if self.empty() == True:
            raise ValueError
        else:
            return self.data[-1]
        """
        Return the element on the top of stack
        :return: the element that is on the top of stack
        """

    def size(self) -> int:
        return len(self.data)
        """
        Return the number of elements in stack
        :return: Number of elements in stack
        """

    def empty(self) -> bool:
        return bool(not self.data)
        """
        Return whether stack is empty or not
        :return: True if stack does not contain any elements
                 False if stack contains elements
        """
        