"""
Programming for linguists

Implementation of the data structure "Queue"
"""

from typing import Iterable


# pylint: disable=invalid-name
class Queue_:
    """
    Queue Data Structure
    """

    def __init__(self, data: Iterable = (), max_size: int = None):
        if not data:
            self.data = []
        else:
            self.data = list(data)
        if not max_size:
            self.max_size = None
        else:
            self.max_size = max_size
            self.data = list(data[:max_size])

    def put(self, element):
        if self.full():
            raise AssertionError
        else:    
            self.data.append(element)
            
        """
        Add the element ‘element’ at the end of queue_
        :param element: element to add to queue_
        """

    def get(self):
        try:
            return self.data.pop(0)
        except IndexError:
            raise AssertionError
        """
        Remove and return an item from queue_
        """

    def empty(self) -> bool:
        return bool(not self.data)
        """
        Return whether queue_ is empty or not
        :return: True if queue_ does not contain any elements.
                 False if the queue_ contains elements
        """

    def full(self) -> bool:
        return bool(len(self.data) == self.max_size)
        """
        Return whether queue_ is full or not
        :return: True if queue_ is full.
                 False if queue_ is not full
        """

    def size(self) -> int:
        return len(self.data) 
        """
        Return the number of elements in queue_
        :return: Number of elements in queue_
        """

    def top(self):
        return self.data[-1]
        """
        Return the number of elements in queue_
        :return: Number of elements in queue_
        """

    def capacity(self) -> int:
        return self.max_size 
        """
        Return the maximal size of queue_
        :return: Maximal size of queue_
        """
