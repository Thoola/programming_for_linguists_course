"""
Programming for linguists

Implementation of the Reverse Polish Notation Converter
"""
from calculator.operator_ import Operator_
from calculator.reverse_polish_notation import ReversePolishNotation
from stack.stack import Stack


class ReversePolishNotationCalculator:
    """
    Calculator of expression in Reverse Polish Notation
    """
    def __init__(self):
        self.value = None
        self.stack = Stack([])

    def calculate(self, rpn_expression: ReversePolishNotation) -> float:
        """
        Main method of the ReversePolishNotationCalculator class.
        Calculate a result of expression in Reverse Polish Notation.

        :param rpn_expression: expression in Reverse Polish Notation Format
        :return:
        """
        while rpn_expression:
            element = rpn_expression.get_next_element()
            if isinstance(element, float):
                self.stack.push(element)
            elif isinstance(element, Operator_):
                operand1, opperand2 = self.get_operand_from_stack(), self.get_operand_from_stack()
                self.stack.push(self.calculate_value(element, operand1, operand1))
        return self.stack.top()

    def get_operand_from_stack(self):
        operand = self.stack.top()
        self.stack.pop()
        return operand

    @staticmethod
    def calculate_value(operator: Operator_, operand_1, operand_2):
        return operator.function(operand_2, operand_1)
