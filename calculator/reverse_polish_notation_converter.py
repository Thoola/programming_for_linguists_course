"""
Programming for linguists

Implementation of the Reverse Polish Notation Converter
"""

from calculator.reverse_polish_notation import ReversePolishNotation
from calculator.operator_ import get_operator_priority
from stack.stack import Stack


class ReversePolishNotationConverter:
    """
    Class for converting infix expressions to reverse polish notation
    """
    point = '.'

    def __init__(self):
        self._stack_operators = Stack([])
        self._rpn = ReversePolishNotation([])
        self._digit = ''

    def convert(self, infix_string: str) -> ReversePolishNotation:
        """
        Main method of the class.
        Convert infix expression to reverse polish notation format

        :param infix_string: expression in infix notation
        :return: ReversePolishNotation object
        """
        

    def pop_from_stack_until_opening_bracket(self):
        pass

    def pop_from_stack_until_prioritizing(self, operator_character: str):
        pass

    def read_digit(self, character: str):
        pass
    @staticmethod
    def is_part_of_digit(character: str) -> bool:
        pass

    @staticmethod
    def is_opening_bracket(character: str) -> bool:
        pass

    @staticmethod
    def is_closing_bracket(character: str) -> bool:
        pass

    @staticmethod
    def is_binary_operation(character: str) -> bool:
        pass
