"""
Programming for linguists

Implementation of the Reverse Polish Notation Converter
"""
import unittest
from typing import Iterable

from calculator.operator_ import Plus, Operator_, Multiplier, Power
from calculator.reverse_polish_notation_converter import ReversePolishNotationConverter


#@unittest.skip
class SimpleReversePolishNotationConverterTestCase(unittest.TestCase):
    def test_const_expression(self):
        expression = '1'
        rpn = ReversePolishNotationConverter().convert(expression)
        element = next(rpn)
        self.assertEqual(element, float(expression))

    def test_simple_expression(self):
        expression = '1+ 2'
        expected_expression = (1.0, 2.0, Plus())
        rpn = ReversePolishNotationConverter().convert(expression)
        actual = tuple(rpn)
        self.check_rpn(actual, expected_expression)

    def test_expression_with_two_operators(self):
        expression = '1+2*3'
        expected_expression = (1.0, 2.0, 3.0, Multiplier(), Plus())
        rpn = ReversePolishNotationConverter().convert(expression)
        actual = tuple(rpn)
        self.check_rpn(actual, expected_expression)

    def test_expression_with_bracket(self):
        expression = '(1+2)*(3)'
        expected_expression = (1.0, 2.0, Plus(), 3.0, Multiplier())
        rpn = ReversePolishNotationConverter().convert(expression)
        actual = tuple(rpn)
        self.check_rpn(actual, expected_expression)

    def test_expression_with_wrong_brackets(self):
        expression = '(1+2)*3)'
        self.assertRaises(Exception, ReversePolishNotationConverter().convert, expression)

    def test_expression_with_power(self):
        expression = '1^2^3^4^5'
        expected_expression = (1.0, 2.0, Power(), 3.0, Power(), 4.0, Power(), 5.0, Power())
        rpn = ReversePolishNotationConverter().convert(expression)
        actual = tuple(rpn)
        self.check_rpn(actual, expected_expression)

    def test_expression_with_power_with_breaks(self):
        expression = '1^(2*3)^(4+5)'
        expected_expression = (1.0, 2.0, 3.0, Multiplier(), Power(), 4.0, 5.0, Plus(), Power())
        rpn = ReversePolishNotationConverter().convert(expression)
        actual = tuple(rpn)
        self.check_rpn(actual, expected_expression)

    def check_rpn(self, first: Iterable, second):
        for index, element in enumerate(first):
            if isinstance(element, float):
                self.assertEqual(element, second[index])
            elif isinstance(element, Operator_):
                self.assertIsInstance(second[index], type(element))
