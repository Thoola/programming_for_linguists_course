"""
Programming for linguists

Class for Reverse Polish Notation
"""

from typing import Optional
from calculator.operator_ import OperatorCreator
from queue_.queue_ import Queue_


class ReversePolishNotation:
    """
    Reverse Polish Notation class
    """

    def __init__(self, expression: list):
        self._rpn = Queue_(expression)

    def __len__(self):
        return self._rpn.size()

    def __iter__(self):
        return self

    def __next__(self):
        if not self._rpn.size():
            raise StopIteration
        return self.get_next_element()

    def get_next_element(self) -> Optional[str]:
        """
        Get next element in Reverse Polish Notation
        :return: next element from rpn if exists or None
        """
        try:
            return self._rpn.get()
        except AssertionError:
            return None

    def put_number(self, number: str):
        """
        Put number to the RPN - getting an argument number as string type and save as float
        :param number: digit in string type
        """
        self._rpn.put(float(number))

    def put_operator(self, operator: str):
        """
        Put operator to the RPN - getting an argument operator as string type and save as Operator_
        :param operator: operator in string type
        """
        self._rpn.put(OperatorCreator.create_operator(operator))
