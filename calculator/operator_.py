"""
Programming for linguists

Classes and functions for Operators
"""

from typing import Optional


class Operator_:
    priority = None


class Plus(Operator_):
    priority = 1
    @staticmethod
    def function(first: float, second: float):
        return first + second


class Minus(Operator_):
    priority = 1
    @staticmethod
    def function(first: float, second: float):
        return first - second


class Multiplier(Operator_):
    priority = 2
    @staticmethod
    def function(first: float, second: float):
        return first * second


class Divider(Operator_):
    priority = 2
    @staticmethod
    def function(first: float, second: float):
        return first / second


class Power(Operator_):
    priority = 3
    @staticmethod
    def function(first: float, second: float):
        return first ** second


class OpeningBracket(Operator_):
    priority = 0


class OperatorCreator:
    @staticmethod
    def create_operator(operator: str):
        ops = {'+': Plus(), '-': Minus(), '*': Multiplier(),'/': Divider(), '^': Power(), '(': OpeningBracket()}
        if operator in ops:
            return operators[operator]
        return None


def get_operator_priority(operator: str) -> int:
    return OperatorCreator.create_operator(operator).priority
