"""
Programming for linguists

Tests for Operators classes and functions.
"""
import unittest

from calculator.operator_ import Plus, Minus, Multiplier, Divider, Power, OperatorCreator, get_operator_priority


#@unittest.skip
class OperatorTestCase(unittest.TestCase):
    def test_plus(self):
        argument_1 = 1
        argument_2 = 2
        self.assertEqual(Plus.function(argument_1, argument_2), Plus.function(argument_2, argument_1))
        self.assertEqual(Plus.function(argument_2, argument_1), argument_1 + argument_2)

    def test_minus(self):
        argument_1 = 1
        argument_2 = 2
        self.assertNotEqual(Minus.function(argument_1, argument_2), Minus.function(argument_2, argument_1))
        self.assertEqual(Minus.function(argument_1, argument_2), argument_1 - argument_2)
        self.assertEqual(Minus.function(argument_2, argument_1), argument_2 - argument_1)

    def test_multiple(self):
        argument_1 = 3
        argument_2 = 2
        self.assertEqual(Multiplier.function(argument_1, argument_2), Multiplier.function(argument_1, argument_2))
        self.assertEqual(Multiplier.function(argument_1, argument_2), argument_1 * argument_2)

    def test_division(self):
        argument_1 = 4
        argument_2 = 2
        self.assertNotEqual(Divider.function(argument_1, argument_2), Divider.function(argument_2, argument_1))
        self.assertEqual(Divider.function(argument_1, argument_2), argument_1 / argument_2)
        self.assertEqual(Divider.function(argument_2, argument_1), argument_2 / argument_1)

    def test_power(self):
        argument_1 = 3
        argument_2 = 2
        self.assertEqual(Power.function(argument_1, argument_2), argument_1 ** argument_2)
        self.assertEqual(Power.function(argument_2, argument_1), argument_2 ** argument_1)

    def test_divider_multiplier_feature(self):
        argument_1 = 4
        argument_2 = 2
        division_result = Divider.function(argument_1, argument_2)
        self.assertEqual(Multiplier.function(division_result, argument_2), argument_1)

    def test_plus_minus_feature(self):
        argument_1 = 4
        argument_2 = 2
        minus_result = Minus.function(argument_1, argument_2)
        self.assertEqual(Plus.function(minus_result, argument_2), argument_1)


@unittest.skip
class OperatorCreationTestCase(unittest.TestCase):
    def test_create_plus(self):
        operator = '+'
        self.assertIsInstance(OperatorCreator.create_operator(operator), Plus)

    def test_create_minus(self):
        operator = '-'
        self.assertIsInstance(OperatorCreator.create_operator(operator), Minus)

    def test_create_multiplier(self):
        operator = '*'
        self.assertIsInstance(OperatorCreator.create_operator(operator), Multiplier)

    def test_create_divider(self):
        operator = '/'
        self.assertIsInstance(OperatorCreator.create_operator(operator), Divider)

    def test_create_power(self):
        operator = '^'
        self.assertIsInstance(OperatorCreator.create_operator(operator), Power)


@unittest.skip
class OperatorPriorityTestCase(unittest.TestCase):
    def test_priority(self):
        plus_priority = get_operator_priority('+')
        minus_priority = get_operator_priority('-')
        multiplier_priority = get_operator_priority('*')
        divider_priority = get_operator_priority('/')
        power_priority = get_operator_priority('^')
        self.assertEqual(plus_priority, minus_priority)
        self.assertLess(minus_priority, multiplier_priority)
        self.assertEqual(multiplier_priority, divider_priority)
        self.assertLess(divider_priority, power_priority)
