"""
Programming for linguists

Tests for class BinaryTree.
"""
import unittest

from binary_search_tree.binary_search_tree import BinarySearchTree
from binary_search_tree.node import Node


#@unittest.skip
class BinaryTreeTestCase(unittest.TestCase):
    """
    This Case of tests checks the functionality of the implementation of BinaryTree
    """

    def test_new_tree_is_empty(self):
        """
        Create an empty BinaryTree.
        Check that its root is None.
        """
        tree_name = 'Test tree'
        tree = BinarySearchTree(tree_name)
        self.assertIsNone(tree.root)
        self.assertEqual(tree.name, tree_name)
        self.assertEqual(tree.depth, 0)

    def test_add_new_node_to_empty_tree(self):
        """
        Create an empty BinaryTree.
        Add new node to the tree.
        Check that the root of the tree is the added Node.
        """
        tree = BinarySearchTree()
        node = Node(1)
        tree.insert(node)
        self.assertEqual(tree.root, node)

    def test_add_new_node_to_not_empty_tree(self):
        """
        Create an empty BinaryTree.
        Add multiple Nodes to the tree.
        Check that the structure of the tree is correct.
        """
        tree = BinarySearchTree()
        node_1 = Node(1)
        node_2 = Node(2)
        node_4 = Node(4)
        node_05 = Node(0.5)
        node_07 = Node(0.7)
        tree.insert(node_1)
        tree.insert(node_2)
        tree.insert(node_05)
        tree.insert(node_07)
        tree.insert(node_4)
        self.assertIs(tree.root, node_1)
        self.assertIs(tree.root.left_node, node_05)
        self.assertIs(node_05.right_node, node_07)
        self.assertEqual(tree.root.right_node, node_2)
        self.assertEqual(node_2.right_node, node_4)

    def test_get_error_new_node_to_tree(self):
        """
        Create an empty BinaryTree.
        Add new Node to the tree.
        Check that adding another Node with the same value raises Error.
        """
        tree = BinarySearchTree()
        node_1 = Node(1)
        node_2 = Node(1)
        tree.insert(node_1)
        self.assertRaises(AssertionError, tree.insert, node_2)

    def test_find_node(self):
        """
        Create an empty BinaryTree.
        Add multiple Nodes to the tree.
        Check that the 'find_node' method returns the correct Node.
        """
        tree = BinarySearchTree()
        node_1 = Node(1)
        node_2 = Node(2)
        node_4 = Node(4)
        node_05 = Node(0.5)
        node_07 = Node(0.7)
        tree.insert(node_1)
        tree.insert(node_2)
        tree.insert(node_05)
        tree.insert(node_07)
        tree.insert(node_4)
        test_node = tree.find_node(0.7)
        self.assertIs(test_node, node_07)

    def test_not_find_node(self):
        """
        Create an empty BinaryTree.
        Add multiple Nodes to the tree.
        Check that the 'find_node' method returns None for not existing Node.
        """
        tree = BinarySearchTree()
        node_1 = Node(1)
        node_2 = Node(2)
        node_4 = Node(4)
        node_05 = Node(0.5)
        node_07 = Node(0.7)
        tree.insert(node_1)
        tree.insert(node_2)
        tree.insert(node_05)
        tree.insert(node_07)
        tree.insert(node_4)
        test_node = tree.find_node(0.76)
        self.assertIsNone(test_node)

    def test_depth(self):
        """
        Create an empty BinaryTree.
        Add multiple Nodes to the tree.
        Check that the depth of the tree equals to the depth of its longest subtree.
        """
        tree = BinarySearchTree()
        tree.insert(Node(1))
        tree.insert(Node(2))
        tree.insert(Node(0.5))
        tree.insert(Node(0.7))
        tree.insert(Node(4))
        tree.insert(Node(5))
        self.assertEqual(tree.depth, 3)

    def test_remove_nonexistent_node(self):
        """
        Create an empty BinaryTree.
        Add multiple Nodes to the tree.

        """
        tree = BinarySearchTree()
        tree.insert(Node(1))
        tree.insert(Node(0))
        tree.insert(Node(2))
        # Should it raise Error?
        tree.remove(5)
        root = tree.root
        self.assertEqual(root.value, 1)
        self.assertEqual(root.left_node.value, 0)
        self.assertEqual(root.right_node.value, 2)

    def test_remove_node_with_right_subtree(self):
        """
        Create an empty BinaryTree.
        Add two Nodes to the tree.
        Remove one of the previously added Nodes.
        Check that the root became the other Node - and it has no Nodes.
        """
        tree = BinarySearchTree()
        tree.insert(Node(1))
        tree.insert(Node(2))
        tree.remove(1)
        root = tree.root
        self.assertEqual(root.value, 2)
        self.assertIsNone(root.left_node)
        self.assertIsNone(root.right_node)

    def test_remove_node_with_left_subtree(self):
        """
        Create an empty BinaryTree.
        Add two Nodes to the tree.
        Remove one of the previously added Nodes.
        Check that the root became the other Node - and it has no Nodes.
        """
        tree = BinarySearchTree()
        tree.insert(Node(2))
        tree.insert(Node(1))
        tree.remove(2)
        root = tree.root
        self.assertEqual(root.value, 1)
        self.assertIsNone(root.left_node)
        self.assertIsNone(root.right_node)

    def test_remove_node(self):
        """
        Create an empty BinaryTree.
        Add multiple Nodes to the tree.
        Remove one of the previously added Nodes.
        Check that the structure of remaining tree is correct.
        """
        tree = BinarySearchTree()
        for value in (10, 5, 3, 1, 4, 7, 6, 8, 15, 13, 17, 11, 14, 12, 16, 18):
            tree.insert(Node(value))
        tree.remove(10)
        root = tree.root
        self.assertEqual(root.value, 11)
        self.check_full_tree_depth_3(root.left_node, (5, 3, 1, 4, 7, 6, 8))
        self.check_full_tree_depth_3(root.right_node, (15, 13, 12, 14, 17, 16, 18))

    def test_traversal(self):
        """
        Create an empty BinaryTree.
        Add multiple Nodes to the tree.
        Check that the 'traverse' method returns the sorted tree.
        """
        tree = BinarySearchTree()
        test_values = (10, 5, 3, 1, 4, 7, 6, 8, 15, 13, 17, 11, 14, 12, 16, 18)
        for value in test_values:
            tree.insert(Node(value))
        values = tree.traverse()
        self.assertTupleEqual(values, tuple(sorted(test_values)))

    def test_revers_traversal(self):
        """
        Create an empty BinaryTree.
        Add multiple Nodes to the tree.
        Check that the 'traverse' method with 'reverse=True' returns the reversed tree.
        """
        tree = BinarySearchTree()
        test_values = (10, 5, 3, 1, 4, 7, 6, 8, 15, 13, 17, 11, 14, 12, 16, 18)
        for value in test_values:
            tree.insert(Node(value))
        values = tree.traverse(reverse=True)
        self.assertTupleEqual(values, tuple(sorted(test_values, reverse=True)))

    def check_full_tree_depth_3(self, root: Node, values: tuple):
        """
        Test utils function to check subtree structure.
        :param root: root of the tree
        :param values: values od the nodes
        """
        self.assertEqual(root.value, values[0])
        self.assertEqual(root.left_node.value, values[1])
        self.assertEqual(root.left_node.left_node.value, values[2])
        self.assertIsNone(root.left_node.left_node.left_node)
        self.assertIsNone(root.left_node.left_node.right_node)
        self.assertEqual(root.left_node.right_node.value, values[3])
        self.assertIsNone(root.left_node.right_node.left_node)
        self.assertIsNone(root.left_node.right_node.right_node)
        self.assertEqual(root.right_node.value, values[4])
        self.assertEqual(root.right_node.left_node.value, values[5])
        self.assertIsNone(root.right_node.left_node.left_node)
        self.assertIsNone(root.right_node.left_node.right_node)
        self.assertEqual(root.right_node.right_node.value, values[6])
        self.assertIsNone(root.right_node.right_node.left_node)
        self.assertIsNone(root.right_node.right_node.right_node)
