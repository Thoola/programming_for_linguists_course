"""
Programming for linguists

Implementation of the Node abstraction for data structure Binary Tree
"""


class Node:
    """
    The Node abstraction
    """
    def __init__(self, value, root=None, left_node=None, right_node=None):
        self.root = root
        self.left_node = left_node
        self.right_node = right_node
        self.value = value
        if self.root == None:
            self.root = self.value
        elif self.value == self.root:
            self.left_node = self.value
            self.right_node = self.value
        elif value < root:
            self.left_node = self.value
        elif value > root:
            self.right_node = self.value

    def __lt__(self, other) -> bool:
        if self.root < other:
            return True
        else:
            return False
        """
        Overload of the < operator
        :param other: an instance of Node to compare with
        :return: True if self < value
                 False otherwise
        """

    def __gt__(self, other) -> bool:
        if self.root > other:
            return True
        else:
            return False
        """
        Overload of the > operator
        :param other: an instance of Node to compare with
        :return: True if self > value
                 False otherwise
        """
