"""
Programming for linguists

Tests for class Node.
"""
import unittest

from binary_search_tree.node import Node


#@unittest.skip
class NodeTestCase(unittest.TestCase):
    """
    This Case of tests checks the functionality of the implementation of Node
    """

    def test_new_node_is_empty(self):
        """
        Create a Node.
        Check the value of the Node.
        """
        test_value = 1
        node = Node(test_value)
        self.assertEqual(node.value, test_value)

    def test_link_three_nodes(self):
        """
        Create multiple Nodes, connect them.
        Check the structure.
        """
        test_value_1 = 1
        test_value_2 = 2
        test_value_3 = 3
        node_1 = Node(test_value_1)
        node_3 = Node(test_value_3)
        node_2 = Node(test_value_2, left_node=node_1, right_node=node_3)
        self.assertEqual(node_2.value, test_value_2)
        self.assertIs(node_2.left_node, node_1)
        self.assertEqual(node_2.right_node, node_3)

    def test_compare_nodes_with_lt(self):
        """
        Create two Nodes.
        Check < operator.
        """
        test_value_1 = 1
        test_value_2 = 2
        node_1 = Node(test_value_1)
        node_2 = Node(test_value_2)
        self.assertLess(node_1, node_2)
        self.assertGreater(node_2, node_1)

    def test_compare_nodes_with_gt(self):
        """
        Create two Nodes.
        Check > operator.
        """
        test_value_1 = 1
        test_value_2 = 2
        node_1 = Node(test_value_1)
        node_2 = Node(test_value_2)
        self.assertLess(node_1, node_2)
        self.assertGreater(node_2, node_1)
