"""
Programming for linguists

Implementation of the Binary Search Tree Data Structure
"""

from typing import Union

from binary_search_tree.node import Node


class BinarySearchTree:
    """
    Binary Search Tree Data Structure
    """

    def __init__(self, name: str = None):
        self.name = name
        self.root = None

    def insert(self, node: Node):
        if not node:
            pass
        elif self.root == node:
            raise ValueError
        else:
            self.root = node


        """
        Insert a Node to the Binary Search Tree.
        Raise Error if a node with the same value already exists in the Tree
        :param node: an instance of Node to add
        """

    def find_node(self, value) -> Union[Node, None]:
        def search (node, value):
            if node == value:
                return True
        if self.root == value:
            return self.root
        elif self.root.left_node == value:
            return self.root.left_node
        elif self.root.left_node > value:
            left_search = search(self.root.left_node)
            if left_search:
                return True
        elif self.root.right_node == value:
            return self.root.right_node
        elif self.root.left_node > value:
            right_search = search(self.root.left_node)
            if right_search:
                return True
        else:
            return None
        """
        Find node with the value `value`
        :param value: the value of Node to search
        :return: an instance of Node from the Tree if the Node is present in the Tree
                 otherwise None
        """

    @property
    def depth(self) -> int:
        """
        Get depth of the tree
        :return: int - depth of the tree
        """

    def remove(self, value):
        """
        Remove node with value `value`
        :param value: value of the node to remove
        """

    def traverse(self, reverse: bool = False) -> tuple:
        """
        Get tuple of values of the tree
        :param reverse: whether to return values in reversed order or not
        :return: tuple of values from the Tree
        """
